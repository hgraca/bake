#!/usr/bin/env bash

if [ "${SUDO_USER}" = '' ]; then
  GLOBAL_BASE_PATH="${HOME}"
else
  GLOBAL_BASE_PATH="/home/${SUDO_USER}"
fi

VENDOR_PATH=./vendor
LOCAL_BAKE_PATH="${VENDOR_PATH}/bin/bake"
GLOBAL_BAKE_PATH="${GLOBAL_BASE_PATH}/.composer/vendor/bin/bake"

if [ -e "${LOCAL_BAKE_PATH}" ]; then
  BAKE_PATH="${LOCAL_BAKE_PATH}"
elif [ -e "${GLOBAL_BAKE_PATH}" ]; then
  BAKE_PATH="${GLOBAL_BAKE_PATH}"
else
  echo "ERROR: can not find bake neither in '${LOCAL_BAKE_PATH}' nor '${GLOBAL_BAKE_PATH}'"
  exit 1
fi

# shellcheck source=/dev/null
. "${BAKE_PATH}"
