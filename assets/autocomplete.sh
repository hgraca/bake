#!/usr/bin/env bash

# @see https://iridakos.com/programming/2018/03/01/bash-programmable-completion-tutorial

_bake_completions()
{
  if [ "${#COMP_WORDS[@]}" != "2" ]; then
    return
  fi

  LOCAL_VENDOR_PATH="./vendor"
  GLOBAL_VENDOR_PATH="$(composer -n config --global home)/vendor"

  if [ -e "${LOCAL_VENDOR_PATH}" ]; then
    VENDOR_PATH="${LOCAL_VENDOR_PATH}"
  elif [ -e "${GLOBAL_VENDOR_PATH}" ]; then
    VENDOR_PATH="${GLOBAL_VENDOR_PATH}"
  else
    echo "ERROR: can not find bake neither in '${LOCAL_VENDOR_PATH}' nor '${GLOBAL_VENDOR_PATH}'"
    exit 1
  fi
  AUTOLOADER_PATH="${VENDOR_PATH}/autoload.sh"

  . "${AUTOLOADER_PATH}" > /dev/null

  BAKE_PATH=".bake"
  RECIPES_PATH="${BAKE_PATH}/recipes"
  BOOTSTRAP_PATH="${BAKE_PATH}/bootstrap.sh"
  if [ -e "${BOOTSTRAP_PATH}" ] && [ ! -d "${BOOTSTRAP_PATH}" ]; then
    . "${BOOTSTRAP_PATH}"
  fi
  if [ -d "${RECIPES_PATH}" ]; then
    for FILE in "${RECIPES_PATH}"/*.sh; do
      . "${FILE}"
    done
  fi
  if [ -d "${RECIPES_PATH}/${ENV}" ]; then
    for FILE in "${RECIPES_PATH}/${ENV}"/*.sh; do
      . "${FILE}"
    done
  fi

  # This will return function names that are in memory, even functions that we have removed from files but are still in memory
  # Start a new shell session if you want to get rid of them
  COMPREPLY=( $(compgen -W "$(bake.available_targets_as_string)" -- "${COMP_WORDS[1]}") )
}

complete -F _bake_completions bake
