# Bake

Bake is a `Make like` build system, built in bash.

The intent is to make it easier and more flexible than Make.

## How to use

### Install

Install with `composer require hgraca\bake`.

You will need to accept a few plugins so that bash scripts can be managed using composer.

You can install bake autocomplete and global link by running `sudo ./vendor/bin/bake install`

#### Global installation

Alternatively, you can also install it globally (tested on Ubuntu based distributions).

Install with `composer global require hgraca\bake`.

You will need to accept a few plugins so that bash scripts can be managed using composer.

You can install bake autocomplete and global link by running `sudo ~/.composer/vendor/bin/bake install`

### Set up bake home

The bake home, bust be `.bake`, on the root of your project.

You can place a bootstrap file, `bootstrap.sh`,  in the root of bakes' home.

The recipes must live under `.bake/recipes/...`.

Optionally, you can create folders that will be included according to the environment variable `ENV`, 
which you can set in the `bootstrap.sh`.

In `bootstrap.sh` you can also set the logging level. You can find all the logging levels [here](https://gitlab.com/hgraca/bash-overlay/-/blob/f7052462f7058309f8ef6041ef47660c1f13bc98/src/functions/stdout.sh#L14-L23).

Example `bootstrap.sh`:

```shell
#!/usr/bin/env bash

ENV='dev'
export BASH_OVERLAY_LOG_LEVEL=${BASH_OVERLAY_LOG_LEVEL_INFO}
```

Example `.bake` structure:

```text
.
├── .bake/
│   ├── bootstrap.sh
│   ├── recipes/
│   │   ├── global-recipes-file-01.sh
│   │   ├── dev/
│   │   │   ├── recipes-file-01.sh
│   │   │   ├── recipes-file-02.sh
│   │   │   └── ...
│   │   └── test/
│   │       ├── recipes-file-03.sh
│   │       ├── recipes-file-04.sh
│   │       └── ...
│   └── whatever-else/
│       └── ...
├── bin
├── src
├── tests
└── ...
```

### Creating and using recipes

In a recipe file you create a function prefixed `bake.recipe.<my_recipe>`, which you then call like `bake <my_recipe>`.

For example, take the following recipe file:

```shell
#!/usr/bin/env bash

########################################################################################################################
# NAMING CONVENTIONS
#
# Please keep the functions ordered alphabetically
#
# Functions names must start with `bake.` so that on the main script we can find the functions available to our "bake".
# Use `.` as a namespace separator (When we double click, it selects only until the `.`)
# Use `-` as a sub separator (When we double click, it selects only until the `-`)
# Use `_` as a word separator (When we double click, it selects including and over the `_`)
#
# Use `.` as the first character in a function name to hide it
#
# At the end of the file place the aliases for backwards compatibility
#
########################################################################################################################

bake.recipe.my_new_command() {
  echo ""
  echo "==============================================="
  echo "===== Hello word, in ${FUNCNAME[0]}, with arguments $* ..."
  echo "==============================================="
}

########################################################################################################################
# Aliases for backwards compatibility
###

bake.recipe.my_old_command() {
  bake.my_new_command "$@"
}
```