#!/usr/bin/env bash

#
# Fills the given variable with an array of available targets
#
# usage:
#   bake.recipe.available_targets TARGETS
#
bake.available_targets() {
  local AVAILABLE_FUNCTIONS_STRING
  declare -n AVAILABLE_TARGETS="$1"

  # This will return function names that are in memory, so even function that we have removed from files but are still in memory
  # Start a new shell session if you want to get rid of them
  AVAILABLE_FUNCTIONS_STRING="$(compgen -A function | grep -E -i "^bake.recipe." | tr '\n' ' ')"
  AVAILABLE_TARGETS=( $(string.replaceText "${AVAILABLE_FUNCTIONS_STRING}" 'bake.recipe.' '') )
}

bake.available_targets_as_string() {
  local TARGETS
  bake.available_targets TARGETS

  echo "${TARGETS[*]}"
}

bake.recipe.help() { ## Show this help
  stdout.log_info "Usage: 'bake <target> [arg1] [arg2] [...]'"
  stdout.log_info "    Available targets:"

  for TARGET in "${AVAILABLE_TARGETS[@]}"; do
    RECIPE_LINE=""
    if [ -e "${RECIPES_PATH}" ]; then
      RECIPE_LINE="$(grep -Eir "^bake.recipe\.${TARGET}\(" ${RECIPES_PATH})"
    fi
    COMMENT=""
    if string.contains "##" "${RECIPE_LINE}"; then
      COMMENT=$(sed 's/^.*## //g' <<< ${RECIPE_LINE})
    fi

    printf "        \033[36m%-50s \033[0m%s\n" "${TARGET}" "${COMMENT}"
  done
}

bake.recipe.install() { ## Install bake
  TARGET_FOLDER="/usr/bin"
  TARGET_FILE="${TARGET_FOLDER}/bake"

  rm -f "${TARGET_FILE}"
  cp -f "${BAKE_INSTALL_PATH}/assets/bake.entrypoint.sh" "${TARGET_FILE}"
  echo "Global entrypoint installed!"

  bake.install-autocomplete
}

bake.install-autocomplete() { ## Install bash autocomplete helper
  TARGET_FOLDER="/etc/bash_completion.d"
  TARGET_FILE="${TARGET_FOLDER}/bake.sh"

  mkdir -p "${TARGET_FOLDER}"
  cp -f "${BAKE_INSTALL_PATH}/assets/autocomplete.sh" "${TARGET_FILE}"

  echo "Autocomplete installed! It will take effect on the next reboot. If you want to try it now, run:"
  echo ". ${TARGET_FILE}"
}
